package com.u1gislab.data.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.u1gislab.data.mapper.DataMapper;
import com.u1gislab.data.vo.renewal.TbDongtan;
import com.u1gislab.data.vo.renewal.TbDongtanModify;
import com.u1gislab.data.vo.renewal.TbDongtanQueue;

@Service
public class DataService {

	@Autowired
	private DataMapper mapper;
	
	public List<TbDongtan> getDongtan(TbDongtan tbDongtan) {
		return mapper.selectDongtan(tbDongtan);
	}
	
	public int addDongtan(TbDongtan tbDongtan) {
		return mapper.insertDongtan(tbDongtan);
	}
	
	public void modifyDongtan(TbDongtan tbDongtan) {
		mapper.updateDongtan(tbDongtan);
	}
	
	public void removeDongtan(TbDongtan tbDongtan) {
		mapper.deleteDongtan(tbDongtan);
	}
	
	
	public List<TbDongtan> getDongtanModify(TbDongtanModify tbDongtanModify) {
		return mapper.selectDongtanModify(tbDongtanModify);
	}
	
	public int addDongtanModify(TbDongtanModify tbDongtanModify) {
		return mapper.insertDongtanModify(tbDongtanModify);
	}
	
	public void modifyDongtanModify(TbDongtanModify tbDongtanModify) {
		mapper.updateDongtanModify(tbDongtanModify);
	}
	
	public void removeDongtanModify(TbDongtanModify tbDongtanModify) {
		mapper.deleteDongtanModify(tbDongtanModify);
	}
	
	
	
	public List<TbDongtan> getDongtanQueue(TbDongtanQueue tbDongtanQueue) {
		return mapper.selectDongtanQueue(tbDongtanQueue);
	}
	
	public int addDongtanQueue(TbDongtanQueue tbDongtanQueue) {
		return mapper.insertDongtanQueue(tbDongtanQueue);
	}
	
	public void modifyDongtanQueue(TbDongtanQueue tbDongtanQueue) {
		mapper.updateDongtanQueue(tbDongtanQueue);
	}
	
	public void removeDongtanQueue(TbDongtanQueue tbDongtanQueue) {
		mapper.deleteDongtanQueue(tbDongtanQueue);
	}
}
