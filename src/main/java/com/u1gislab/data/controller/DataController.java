package com.u1gislab.data.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.u1gislab.data.service.DataService;
import com.u1gislab.data.vo.renewal.TbDongtan;
import com.u1gislab.data.vo.renewal.TbDongtanModify;
import com.u1gislab.data.vo.renewal.TbDongtanQueue;

/**
 * 

### 생성
{
addDongtan id 자동생성
addDongtanModify dongtan table id
addDongtanQueue dongtan table id
}

### 삭제
{
modifyDongtan renewal_type = 2
modifyDongtanModify renewal_type = 2
addDongtanQueue  renewal_type = 2
}

### 수정
{
modifyDongtan renewal_type = 4
modifyDongtanModify renewal_type = 4
addDongtanQueue  renewal_type = 4
}

### 조회
1. getDongtan?date=2020111131123 v = 2
2.getDongtanModify v = 1;

 */
@Controller
public class DataController {

	@Autowired
	private DataService dataService;
	
	
	@GetMapping("/initializeMap")
	@ResponseBody
	public String createMap() throws Exception {
		/*
		createMap("332384.035", "4119110.925", "83.074", "332387.319", "4119111.106", "83.074", "332387.116", "4119112.668", "83.074", "332384.035", "4119112.868", "83.074", 2005372, 3, 999999);
		createMap("332369.891", "4119106.443", "82.957", "332373.175", "4119106.624", "82.957", "332372.972", "4119108.186", "82.957", "332369.891", "4119108.386", "82.957", 2005380, 3, 999999);
		createMap("332213.464", "4119113.723", "80.732", "332216.748", "4119113.904", "80.732", "332216.545", "4119115.466", "80.732", "332213.464", "4119115.666", "80.732", 2005382, 3, 999999);
		createMap("332117.268", "4119120.343", "79.592", "332120.552", "4119120.524", "79.592", "332120.349", "4119122.086", "79.592", "332117.268", "4119122.286", "79.592", 2005380, 3, 999999);
		createMap("332082.178", "4119131.988", "78.922", "332085.462", "4119132.169", "78.922", "332085.259", "4119133.731", "78.922", "332082.178", "4119133.931", "78.922", 2005372, 3, 999999);
		
		createMap("331994.622", "4119161.365", "77.651", "331997.906", "4119161.546", "77.651", "331997.703", "4119163.108", "77.651", "331994.622", "4119163.308", "77.651", 2005373, 3, 999999);
		createMap("331850.041", "4119198.283", "75.721", "331853.325", "4119198.464", "75.721", "331853.122", "4119200.026", "75.721", "331850.041", "4119200.226", "75.721", 2005380, 3, 999999);
		createMap("331824.185", "4119200.944", "75.447", "331827.469", "4119201.125", "75.447", "331827.266", "4119202.687", "75.447", "331824.185", "4119202.887", "75.447", 2005392, 3, 999999);
		createMap("331648.981", "4119240.79",  "72.93",  "331652.265", "4119240.971", "72.93",  "331648.981", "331652.062", "4119242.533", "72.93", "4119242.733", "72.93", 2005373, 3, 999999);
		createMap("331638.992", "4119239.161", "72.881", "331642.276", "4119239.342", "72.881", "331638.992", "331642.073", "4119240.904", "72.881", "4119241.104", "72.881", 2005380, 3, 999999);
		
		createMap("331601.862", "4119248.963", "72.553", "331605.146", "4119249.144", "72.553", "331604.943", "4119250.706", "72.553", "331601.862", "4119250.906", "72.553", 2005382, 3, 999999);
		createMap("331447.306", "4119268.277", "72.033", "331450.59",  "4119268.458", "72.033", "331450.387", "4119270.02", "72.033", "331447.306", "4119270.22", "72.033", 2005392, 3, 999999);
		createMap("331271.444", "4119227.5",   "71.528", "331271.485", "4119230.009", "71.528", "331270.102", "4119230.023", "71.528", "331270.102", "4119227.5", "71.528", 2005372, 3, 999999);
		createMap("331274.543", "4119212.122", "71.631", "331274.584", "4119214.631", "71.631", "331273.201", "4119214.645", "71.631", "331273.201", "4119212.122", "71.631", 2005380, 3, 999999);
		createMap("331267.587", "4119195.173", "71.596", "331267.628", "4119197.682", "71.596", "331266.245", "4119197.696", "71.596", "331266.245", "4119195.173", "71.596", 2005382, 3, 999999);
		
		createMap("331270.879", "4119036.082", "72.271", "331270.92",  "4119038.591", "72.271", "331269.537", "4119038.605", "72.271", "331269.537", "4119036.082", "72.271", 2005370, 3, 999999);
		createMap("331266.911", "4118855.734", "72.771", "331266.952", "4118858.243", "72.771", "331265.569", "4118858.257", "72.771", "331265.569", "4118855.734", "72.771", 2005380, 3, 999999);
		createMap("331263.036", "4118832.399", "72.718", "331263.077", "4118834.908", "72.718", "331261.694", "4118834.922", "72.718", "331261.694", "4118832.399", "72.718", 2005372, 3, 999999);
		createMap("331258.839", "4118774.343", "72.846", "331258.88",  "4118776.852", "72.846", "331257.497", "4118776.866", "72.846", "331257.497", "4118774.343", "72.846", 2005382, 3, 999999);
		createMap("331257.103", "4118694.682", "72.673", "331257.144", "4118697.191", "72.673", "331255.761", "4118697.205", "72.673", "331255.761", "4118694.682", "72.673", 2005373, 3, 999999);
		*/
		
		createMap("332067.283", "4119136.719", "78.761", "332071.17", "4119135.502", "78.817", "332067.449", "4119138.012", "78.727", "332071.513", "4119136.755", "78.779", 2005372, 3, 999999);
		createMap("331506.219", "4119265.711", "72.096", "331509.89",  "4119265.127", "72.115", "331506.177", "4119267.357", "72.062", "331510.19", "4119266.609", "72.078", 2005373, 3, 999999);
		createMap("331249.448", "4118504.128", "74.437", "", "", "", "", "", "", "", "", "", 2001310, 1, 999999);
		
		return "{\"result\":\"success\"}";
	}
	
	
	/**
	 * 맵생성
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/createMap")
	@ResponseBody
	public String createMap(@RequestParam(value="lsx", defaultValue = "") String lsx, 
			@RequestParam(value="lsy", defaultValue = "") String lsy,
			@RequestParam(value="lsz", defaultValue = "") String lsz,
			@RequestParam(value="lex", defaultValue = "") String lex,
			@RequestParam(value="ley", defaultValue = "") String ley,
			@RequestParam(value="lez", defaultValue = "") String lez,
			@RequestParam(value="rsx", defaultValue = "") String rsx, 
			@RequestParam(value="rsy", defaultValue = "") String rsy,
			@RequestParam(value="rsz", defaultValue = "") String rsz,
			@RequestParam(value="rex", defaultValue = "") String rex,
			@RequestParam(value="rey", defaultValue = "") String rey,
			@RequestParam(value="rez", defaultValue = "") String rez,
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		StringBuilder sb = new StringBuilder();
		sb.append("lsx:" + lsx);
		sb.append(",lsy:" + lsx);
		sb.append(",lsz:" + lsx);
		sb.append(",lex:" + lsx);
		sb.append(",ley:" + lsx);
		sb.append(",lez:" + lsx);
		sb.append(",rsx:" + lsx);
		sb.append(",rsy:" + lsx);
		sb.append(",rsz:" + lsx);
		sb.append(",rex:" + lsx);
		sb.append(",rey:" + lsx);
		sb.append(",rez:" + lsx);
		sb.append(",layerId:" + layerId);
		sb.append(",commonCode:" + commonCode);
		sb.append(",renewalType:" + renewalType);
		
		System.out.println("createMap ::: " + sb.toString());
		
		
		//addDongtan
		TbDongtan dongtan = new TbDongtan();
		if (commonCode == 1) {
			dongtan.setSquarePoints("POINT Z ("+ lsx + " " + lsy + " " + lsz + ")");	
		} else if (commonCode == 2) {
			dongtan.setSquarePoints("LINESTRING Z (" + lsx + " " + lsy +" " + lsz + "," + lex + " " + ley + " " + lez + ")");
		} else if (commonCode == 3) {
			dongtan.setSquarePoints("POLYGON Z (("+
					lsx + " " + lsy + " " + lsz + "," +
					lex + " " + ley + " " + lez + "," +
					rsx + " " + rsy + " " + rsz + "," +
					rex + " " + rey + " " + rez + "," +
					lsx + " " + lsy + " " + lsz + "))");
		}
		
		dongtan.setLayerId(layerId);
		dongtan.setCommonCode(commonCode);
		if (renewalType < 999999) {
			dongtan.setRenewalType(renewalType);
		}
		int id = dataService.addDongtan(dongtan);
		
		//addDongtanModify
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setId(id);
		dongtanModify.setSquarePoints(dongtan.getSquarePoints());
		dongtanModify.setLayerId(dongtan.getLayerId());
		dongtanModify.setCommonCode(dongtan.getCommonCode());
		dongtanModify.setRenewalType(dongtan.getRenewalType());
		dataService.addDongtanModify(dongtanModify);
		
		//addDongtanQueue
		TbDongtanQueue dongtanQueue = new TbDongtanQueue();
		dongtanQueue.setDongtanId(id);
		dongtanQueue.setSquarePoints(dongtan.getSquarePoints());
		dongtanQueue.setLayerId(dongtan.getLayerId());
		dongtanQueue.setCommonCode(dongtan.getCommonCode());
		dongtanQueue.setRenewalType(dongtan.getRenewalType());
		dataService.addDongtanQueue(dongtanQueue);
		
		return "{\"result\":\"success\"}";
	}
	
	/**
	 * 맵삭제
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/removeMap")
	@ResponseBody
	public String removeMap(@RequestParam(value="id", defaultValue = "9999999") int id) throws Exception {
		
		System.out.println("removeMap ::: id ::: " + id);
		
		TbDongtan dongtan = new TbDongtan();
		dongtan.setId(id);
		dongtan.setRenewalType(2);
		dataService.modifyDongtan(dongtan);
		
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setId(id);
		dongtanModify.setRenewalType(2);
		dataService.modifyDongtanModify(dongtanModify);
		
		List<TbDongtan> dongtanList = dataService.getDongtan(dongtan);
		
		if (dongtanList.size() > 0) {
			TbDongtanQueue dongtanQueue = new TbDongtanQueue();
			dongtanQueue.setDongtanId(dongtanList.get(0).getId());
			dongtanQueue.setSquarePoints(dongtanList.get(0).getSquarePoints());
			dongtanQueue.setCommonCode(dongtanList.get(0).getCommonCode());
			dongtanQueue.setLayerId(dongtanList.get(0).getLayerId());
			dongtanQueue.setRenewalType(dongtanList.get(0).getRenewalType());
			dataService.addDongtanQueue(dongtanQueue);			
		}
		
		return "{\"result\":\"success\"}";
	}
	
	/**
	 * 맵수정
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/modifyMap")
	@ResponseBody
	public String modifyMap(@RequestParam(value="id", defaultValue = "9999999") int id,
			@RequestParam(value="lsx", defaultValue = "") String lsx, 
			@RequestParam(value="lsy", defaultValue = "") String lsy,
			@RequestParam(value="lsz", defaultValue = "") String lsz,
			@RequestParam(value="lex", defaultValue = "") String lex,
			@RequestParam(value="ley", defaultValue = "") String ley,
			@RequestParam(value="lez", defaultValue = "") String lez,
			@RequestParam(value="rsx", defaultValue = "") String rsx, 
			@RequestParam(value="rsy", defaultValue = "") String rsy,
			@RequestParam(value="rsz", defaultValue = "") String rsz,
			@RequestParam(value="rex", defaultValue = "") String rex,
			@RequestParam(value="rey", defaultValue = "") String rey,
			@RequestParam(value="rez", defaultValue = "") String rez,
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode) throws Exception {
		
		
		StringBuilder sb = new StringBuilder();
		sb.append("id:" + id);
		sb.append(",lsx:" + lsx);
		sb.append(",lsy:" + lsx);
		sb.append(",lsz:" + lsx);
		sb.append(",lex:" + lsx);
		sb.append(",ley:" + lsx);
		sb.append(",lez:" + lsx);
		sb.append(",rsx:" + lsx);
		sb.append(",rsy:" + lsx);
		sb.append(",rsz:" + lsx);
		sb.append(",rex:" + lsx);
		sb.append(",rey:" + lsx);
		sb.append(",rez:" + lsx);
		sb.append(",layerId:" + layerId);
		sb.append(",commonCode:" + commonCode);
		
		System.out.println("modifyMap ::: " + sb.toString());
		
		TbDongtan dongtan = new TbDongtan();
		
		if (commonCode == 1) {
			dongtan.setSquarePoints("POINT Z ("+ lsx + " " + lsy + " " + lsz + ")");	
		} else if (commonCode == 2) {
			dongtan.setSquarePoints("LINESTRING Z (" + lsx + " " + lsy +" " + lsz + "," + lex + " " + ley + " " + lez + ")");
		} else if (commonCode == 3) {
			dongtan.setSquarePoints("POLYGON Z (("+
					lsx + " " + lsy + " " + lsz + "," +
					lex + " " + ley + " " + lez + "," +
					rsx + " " + rsy + " " + rsz + "," +
					rex + " " + rey + " " + rez + "," +
					lsx + " " + lsy + " " + lsz + "))");
		}
		
		dongtan.setId(id);
		dongtan.setLayerId(layerId);
		dongtan.setCommonCode(commonCode);
		dongtan.setRenewalType(4);
		dataService.modifyDongtan(dongtan);
		
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setId(id);
		dongtanModify.setSquarePoints(dongtan.getSquarePoints());
		dongtanModify.setLayerId(layerId);
		dongtanModify.setCommonCode(commonCode);
		dongtanModify.setRenewalType(4);
		dataService.modifyDongtanModify(dongtanModify);
		
		
		List<TbDongtan> dongtanList = dataService.getDongtan(dongtan);
		if (dongtanList.size() > 0) {
			TbDongtanQueue dongtanQueue = new TbDongtanQueue();
			dongtanQueue.setDongtanId(dongtanList.get(0).getId());
			dongtanQueue.setSquarePoints(dongtanList.get(0).getSquarePoints());
			dongtanQueue.setCommonCode(dongtanList.get(0).getCommonCode());
			dongtanQueue.setLayerId(dongtanList.get(0).getLayerId());
			dongtanQueue.setRenewalType(dongtanList.get(0).getRenewalType());
			dataService.addDongtanQueue(dongtanQueue);			
		}
		
		return "{\"result\":\"success\"}";
	}
	
	@GetMapping("/getDongtan")
	@ResponseBody
	public String getDongtan(@RequestParam(value="date", defaultValue = "") String date) throws Exception {
		TbDongtan dongtan = new TbDongtan();
		dongtan.setSDate(date);
		return getSelectData(dataService.getDongtan(dongtan), "2", date);
	}
	
	@PostMapping("addDongtan")
	@ResponseBody
	public String addDongtan(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtan dongtan = new TbDongtan();
		dongtan.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtan.setLayerId(layerId);
		if (commonCode != 999999)
			dongtan.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtan.setRenewalType(renewalType);
		
		dataService.addDongtan(dongtan);
		
		return "{\"result\":\"success\"}";
		
	}
	
	@PutMapping("modifyDongtan")
	@ResponseBody
	public String modifyDongtan(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtan dongtan = new TbDongtan();
		dongtan.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtan.setLayerId(layerId);
		if (commonCode != 999999)
			dongtan.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtan.setRenewalType(renewalType);
		
		dataService.modifyDongtan(dongtan);
		
		return "{\"result\":\"success\"}";
	}
	
	@DeleteMapping("removeDongtan") 
	@ResponseBody
	public String removeDongtan(@RequestParam(value="id", defaultValue = "9999999") int id) throws Exception {
		
		TbDongtan dongtan = new TbDongtan();
		dongtan.setId(id);
		dataService.removeDongtan(dongtan);
		
		return "{\"result\":\"success\"}";
	}
	
	@GetMapping("/getDongtanModify")
	@ResponseBody
	public String getDongtanModify(@RequestParam(value="date", defaultValue = "") String date) throws Exception {
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setSDate(date);
		return getSelectData(dataService.getDongtanModify(dongtanModify), "1", date);
	}
	
	@PostMapping("addDongtanModify")
	@ResponseBody
	public String addRenewalHdmap(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtanModify.setLayerId(layerId);
		if (commonCode != 999999)
			dongtanModify.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtanModify.setRenewalType(renewalType);
		
		dataService.addDongtanModify(dongtanModify);
		
		return "{\"result\":\"success\"}";
	}
	
	@PutMapping("modifyDongtanModify")
	@ResponseBody
	public String modifyDongtanModify(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtanModify.setLayerId(layerId);
		if (commonCode != 999999)
			dongtanModify.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtanModify.setRenewalType(renewalType);
		
		dataService.modifyDongtanModify(dongtanModify);
		
		return "{\"result\":\"success\"}";
	}
	
	@DeleteMapping("removeDongtanModify") 
	@ResponseBody
	public String removeDongtanModify(@RequestParam(value="id", defaultValue = "9999999") int id) throws Exception {
		
		TbDongtanModify dongtanModify = new TbDongtanModify();
		dongtanModify.setId(id);
		dataService.removeDongtanModify(dongtanModify);
		
		return "{\"result\":\"success\"}";
	}
	
	@GetMapping("/getDongtanQueue")
	@ResponseBody
	public String getDongtanQueue(@RequestParam(value="date", defaultValue = "") String date) throws Exception {
		TbDongtanQueue dongtanQueue = new TbDongtanQueue();
		dongtanQueue.setSDate(date);
		return getSelectData(dataService.getDongtanQueue(dongtanQueue), "3", date);
	}
	
	@PostMapping("addDongtanQueue")
	@ResponseBody
	public String addDongtanQueue(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtanQueue dongtanQueue = new TbDongtanQueue();
		dongtanQueue.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtanQueue.setLayerId(layerId);
		if (commonCode != 999999)
			dongtanQueue.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtanQueue.setRenewalType(renewalType);
		
		dataService.addDongtanQueue(dongtanQueue);
		
		return "{\"result\":\"success\"}";
	}
	
	@PutMapping("modifyDongtanQueue")
	@ResponseBody
	public String modifyDongtanQueue(@RequestParam(value="squarePoints", defaultValue = "") String squarePoints, 
			@RequestParam(value="layerId", defaultValue = "9999999") int layerId, 
			@RequestParam(value="commonCode", defaultValue = "9999999") int commonCode, 
			@RequestParam(value="renewalType", defaultValue = "9999999") int renewalType) throws Exception {
		
		TbDongtanQueue dongtanQueue = new TbDongtanQueue();
		dongtanQueue.setSquarePoints(squarePoints);
		if (layerId != 999999)
			dongtanQueue.setLayerId(layerId);
		if (commonCode != 999999)
			dongtanQueue.setCommonCode(commonCode);
		if (commonCode != 999999)
			dongtanQueue.setRenewalType(renewalType);
		
		dataService.modifyDongtanQueue(dongtanQueue);
		
		return "{\"result\":\"success\"}";
	}
	
	@DeleteMapping("removeDongtanQueue")
	@ResponseBody
	public String removeDongtanQueue(@RequestParam(value="id", defaultValue = "9999999") int id) throws Exception {
		
		TbDongtanQueue dongtanQueue = new TbDongtanQueue();
		dongtanQueue.setId(id);
		dataService.removeDongtanQueue(dongtanQueue);
		
		return "{\"result\":\"success\"}";
	}
	
	private String getSelectData(List<TbDongtan> tableList, String v, String date) throws JSONException {
		
		JSONArray ja = new JSONArray();
		
		for (TbDongtan td : tableList) {
			int commonCode = td.getCommonCode();
			String square_points = td.getSquarePoints();
			
			int sub_start =0 ;
			int sub_end =0 ;
			String Left_Start_X = "";
			String Left_Start_Y = "";
			String Left_Start_Z = "";
			String Left_End_X   = "";
			String Left_End_Z   = "";
			String Left_End_Y   = "";
			String Right_Start_X = "";
			String Right_Start_Y = "";
			String Right_Start_Z = "";
			String Right_End_X = "";
			String Right_End_Y = "";
			String Right_End_Z = ""; 
			String Left_Start ="";
			String Left_End ="";
			String Right_Start = "";
			String Right_End  = "";
			
			if(commonCode == 1) {
				sub_start = 9;
				sub_end = 1;
				String sub_square_points = square_points.substring(sub_start, square_points.length() - sub_end); 
				Left_Start_X = sub_square_points.substring(0, sub_square_points.indexOf(" "));
				Left_Start_Y = sub_square_points.substring(sub_square_points.indexOf(" ") + 1, sub_square_points.lastIndexOf(" "));
				Left_Start_Z = sub_square_points.substring(sub_square_points.lastIndexOf(" ") + 1, sub_square_points.length());
			} else if(commonCode == 2) {
				sub_start = 14;
				sub_end = 1;
				String sub_square_points = square_points.substring(sub_start, square_points.length() - sub_end); 
				Left_Start = sub_square_points.substring(0, sub_square_points.indexOf(","));
				Left_Start_X = Left_Start.substring(0, Left_Start.indexOf(" "));
				Left_Start_Y = Left_Start.substring(Left_Start.indexOf(" ") + 1, Left_Start.lastIndexOf(" "));
				Left_Start_Z = Left_Start.substring(Left_Start.lastIndexOf(" ") + 1,Left_Start.length());
				Left_End = sub_square_points.substring(sub_square_points.indexOf(",") + 1, sub_square_points.length());
				Left_End_X = Left_End.substring(0, Left_End.indexOf(" "));
				Left_End_Y = Left_End.substring(Left_End.indexOf(" ") + 1, Left_End.lastIndexOf(" "));
				Left_End_Z = Left_End.substring(Left_End.lastIndexOf(" ") + 1, Left_End.length());
			} else if(commonCode == 3) {
				sub_start = 12;
				sub_end = 2;
				String sub_square_points = square_points.substring(sub_start, square_points.length() - sub_end); 
				Left_Start = sub_square_points.substring(0, sub_square_points.indexOf(","));
				Left_Start_X = Left_Start.substring(0, Left_Start.indexOf(" "));
				Left_Start_Y = Left_Start.substring(Left_Start.indexOf(" ") + 1, Left_Start.lastIndexOf(" "));
				Left_Start_Z = Left_Start.substring(Left_Start.lastIndexOf(" ") + 1, Left_Start.length());
				String sub2_square_points = sub_square_points.substring(sub_square_points.indexOf(",") + 1, sub_square_points.length());
				Left_End = sub2_square_points.substring(0, sub2_square_points.indexOf(","));
				Left_End_X = Left_End.substring(0, Left_End.indexOf(" "));
				Left_End_Y = Left_End.substring(Left_End.indexOf(" ") + 1, Left_End.lastIndexOf(" "));
				Left_End_Z = Left_End.substring(Left_End.lastIndexOf(" ") + 1, Left_End.length()) ;
				String sub3_square_points = sub2_square_points.substring(sub2_square_points.indexOf(",") + 1, sub2_square_points.length());
				Right_Start = sub3_square_points.substring(0, sub3_square_points.indexOf(","));
				Right_Start_X = Right_Start.substring(0, Right_Start.indexOf(" "));
				Right_Start_Y = Right_Start.substring(Right_Start.indexOf(" ") + 1, Right_Start.lastIndexOf(" "));
				Right_Start_Z = Right_Start.substring(Right_Start.lastIndexOf(" ") + 1, Right_Start.length()) ;
				String sub4_square_points = sub3_square_points.substring(sub3_square_points.indexOf(",") + 1, sub3_square_points.length());
				Right_End = sub4_square_points.substring(0, sub4_square_points.indexOf(","));
				Right_End_X = Right_End.substring(0, Right_End.indexOf(" "));
				Right_End_Y = Right_End.substring(Right_End.indexOf(" ") + 1, Right_End.lastIndexOf(" "));
				Right_End_Z = Right_End.substring(Right_End.lastIndexOf(" ") + 1, Right_End.length()) ;
			}
			
			JSONObject jo = new JSONObject();

			jo.put("VehicleNum", v);
			jo.put("id", String.valueOf(td.getId()));
			jo.put("Layer_ID", String.valueOf(td.getLayerId()));
			jo.put("Common_code", String.valueOf(td.getCommonCode()));
			
			jo.put("Left_Start_X", Left_Start_X);
			jo.put("Left_Start_Y", Left_Start_Y);
			jo.put("Left_Start_Z", Left_Start_Z);
			jo.put("Left_End_X", Left_End_X);
			jo.put("Left_End_Z", Left_End_Z);
			jo.put("Left_End_Y", Left_End_Y);
			jo.put("Right_Start_X", Right_Start_X);
			jo.put("Right_Start_Y", Right_Start_Y);
			jo.put("Right_Start_Z", Right_Start_Z);
			jo.put("Right_End_X", Right_End_X);
			jo.put("Right_End_Y", Right_End_Y);
			jo.put("Right_End_Z", Right_End_Z);
			
			jo.put("polygon_points", isNull(td.getPolygonPoints()));
			jo.put("W_Date", td.getWDate());
			jo.put("renewal_type", String.valueOf(td.getRenewalType()));
			
			ja.put(jo);
			
		}
		
		JSONObject resultJson = new JSONObject();
		resultJson.put("list", ja);
		
		return resultJson.toString();
	}
	
	private String isNull(String str) {
		if (str == null)
			return "null";
		return str;
	}
}