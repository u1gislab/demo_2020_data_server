package com.u1gislab.data.vo.renewal;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class TbDongtan {
	
	private int id;
	private String squarePoints;
	private String polygonPoints;
	private int layerId;
	private int commonCode;
	
	private int renewalType;
	private boolean gcp;
	private int qi;
	private int linkId1;
	private int linkId1TableName;
	
	private int linkId2;
	private int linkId2TableName;
	private int linkId3;
	private int linkId3TableName;
	private int linkId4;
	
	private int linkId4TableName;
	private int linkId5;
	private int linkId5TableName;
	private int linkId6;
	private int linkId6TableName;
	
	private int linkId7;
	private int linkId7TableName;
	private int linkId8;
	private int linkId8TableName;
	private int linkId9;
	
	private int linkId9TableName;
	private int linkId10;
	private int linkId10TableName;
	private int transcerseSlope;
	private int curvature;
	
	private int relationDiv;
	private int relationId;
	private int objectId;
	private Timestamp wDate;
	private String sDate; //String date
	private Timestamp _version;
	
}
