package com.u1gislab.data.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.u1gislab.data.vo.renewal.TbDongtan;
import com.u1gislab.data.vo.renewal.TbDongtanModify;
import com.u1gislab.data.vo.renewal.TbDongtanQueue;

@Mapper
public interface DataMapper {
	List<TbDongtan> selectDongtan(TbDongtan tbDongtan);
	int insertDongtan(TbDongtan tbDongtan);
	void updateDongtan(TbDongtan tbDongtan);
	void deleteDongtan(TbDongtan tbDongtan);
	
	List<TbDongtan> selectDongtanModify(TbDongtanModify tbDongtanModify);
	int insertDongtanModify(TbDongtanModify tbDongtanModify);
	void updateDongtanModify(TbDongtanModify tbDongtanModify);
	void deleteDongtanModify(TbDongtanModify tbDongtanModify);
	
	List<TbDongtan> selectDongtanQueue(TbDongtanQueue tbDongtanQueue);
	int insertDongtanQueue(TbDongtanQueue tbDongtanQueue);
	void updateDongtanQueue(TbDongtanQueue tbDongtanQueue);
	void deleteDongtanQueue(TbDongtanQueue tbDongtanQueue);
}
